/**
*	client_helpers provides functions to assist the main client in doing basic 
* 	tasks such as writing to the server and handling reading tasks.
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef CLIENT_HELPER_H
#define CLIENT_HELPER_H

#include <netdb.h>
#include <stdbool.h>

#include "mnc_client.h"

/*
 * Gets the server address, whether it is in dotted notation or hostname
 */
void getHostAddress(const char *host, struct in_addr *address);

/**
 * handleServerRead
 * Given the server connection struct, handles reading from the socket and
 * performing any relevent tasks.
 * Returns a bool indicating wether the server is still readable from (i.e.
 * the socket is still open)
 */
bool handleServerRead(ServerConnection *server);

/*
 * Given the server connection struct, attempts to write as much of the buffer
 * to the socket as possible, but only flushing at each new line character.
 * Returns a bool indicating wether the server is stillwritable to (i.e.
 * the socket is still open)
 */
bool handleServerWrite(ServerConnection *server);

/*
 * Handles a read from stdin and any associated tasks including writing the 
 * results to the server
 */
bool handleStdinRead(ServerConnection *server);

/*
 * Checks if the given string is a valid command that can be sent to the
 * server
 */
bool validCommand(char *command);

#endif