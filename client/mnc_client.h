/**
*	mnc_client is the main file in the client, connecting to the server and
*   delegating tasks to handler functions.
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef CLIENT_H
#define CLIENT_H

#include <netinet/in.h>

#include "../helpers/global_config.h"

/*
 * Server connection Struct
 * hold information about the name, port, file descriptor, socket address 
 * and buffers.
 */
typedef struct
{
	const char *serverName;
	int serverFileDesc, serverPort;
	struct sockaddr_in *serverAddr;
	char readBuffer[MAX_BUFFER], writeBuffer[MAX_BUFFER];
} ServerConnection;

/*
 * Enters communication with the server by using a select statement,
 * exiting when the server has closed hown or the client quits.
 */
void enterServerCommunication(ServerConnection *server);

#endif