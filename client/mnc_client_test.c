#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <strings.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <errno.h>
#include <arpa/inet.h>

#include "mnc_client.h"
#include "client_helpers.h"

int main(int argc, char** argv)
{
	int serverFileDesc, serverPort;
	struct sockaddr_in serverAddr;

	bzero(&serverAddr, sizeof(serverAddr));

	if (argc != 3)
	{
		printf("Usage: ./mnc_client server_name server_port\n");
		exit(1);
	}

	serverPort = atoi(argv[2]);

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(serverPort);
	getHostByName(argv[1], &(serverAddr.sin_addr));

	char temp[2048];
	inet_ntop(AF_INET, &serverAddr, temp, sizeof(serverAddr));
	printf("Address: %s\n", temp);

	serverFileDesc = socket(AF_INET, SOCK_STREAM, 0);

	if (connect(serverFileDesc, (struct sockaddr*)&serverAddr, sizeof(serverAddr)) < 0)
	{
		fprintf(stderr, "Could not connect to server: %d\n", errno);
		exit(1);
	}

	char *sendString = "JOIN julz julianhome Julian Carrivick!\n";

	if (send(serverFileDesc, sendString, strlen(sendString), 0) < 0)
	{
		fprintf(stderr, "Could not send Data: %d\n", errno);
		exit(1);
	}

	sendString = "  TIME   \n";

	if (send(serverFileDesc, sendString, strlen(sendString), 0) < 0)
	{
		fprintf(stderr, "Could not send Data: %d\n", errno);
		exit(1);
	}

	sendString = "ALIVE \n";

	if (send(serverFileDesc, sendString, strlen(sendString), 0) < 0)
	{
		fprintf(stderr, "Could not send Data: %d\n", errno);
		exit(1);
	}

	sendString = "   WHOIS julz \n";

	if (send(serverFileDesc, sendString, strlen(sendString), 0) < 0)
	{
		fprintf(stderr, "Could not send Data: %d\n", errno);
		exit(1);
	}

	sendString = "   MSG \n";

	if (send(serverFileDesc, sendString, strlen(sendString), 0) < 0)
	{
		fprintf(stderr, "Could not send Data: %d\n", errno);
		exit(1);
	}

	sendString = "JOIN julz julianhome Julian Carrivick!\n";

	if (send(serverFileDesc, sendString, strlen(sendString), 0) < 0)
	{
		fprintf(stderr, "Could not send Data: %d\n", errno);
		exit(1);
	}

	sendString = "   MSG julz Hi there buddy.\n";

	if (send(serverFileDesc, sendString, strlen(sendString), 0) < 0)
	{
		fprintf(stderr, "Could not send Data: %d\n", errno);
		exit(1);
	}

	sendString = "   MSG Hi everyone. \n";

	if (send(serverFileDesc, sendString, strlen(sendString), 0) < 0)
	{
		fprintf(stderr, "Could not send Data: %d\n", errno);
		exit(1);
	}

	sendString = "   MSG REAAAAAAAAAAAAAAAAAAAAALLLLLLLLLLLLLLLLLLLLLLLLLLLYYYYYYYYYYYYYYYYYYYYYYYYYY \
	 LLLLLLLLLLLLLLLLOOOOOOOOOOOOOOOOOOONNNNNNNNNNNNNNNNNNNNGGGGGGGGGGGGGGGGGG \
	 MMMMMMMMMMMMMMEEEEEEEEEEEEESSSSSSSSSSSSAAAAAAAAAAAAAGGGGGGGGGGGGGGGEEEEEEEEEEE \
	 EEEEEEEEEEEEEEVVVVVVVVVVVVEEEEEEEEEEENNNNNNNNN LLLLLLLLOOOOONNNNNNNNGEEEEEEEEERR\n";

	if (send(serverFileDesc, sendString, strlen(sendString), 0) < 0)
	{
		fprintf(stderr, "Could not send Data: %d\n", errno);
		exit(1);
	}

	sendString = "   QUIT Goodbye everyone. \n";

	if (send(serverFileDesc, sendString, strlen(sendString), 0) < 0)
	{
		fprintf(stderr, "Could not send Data: %d\n", errno);
		exit(1);
	}

	if (shutdown(serverFileDesc, SHUT_WR) < 0)
	{
		fprintf(stderr, "Could not close connection: %d\n", errno);
		close(serverFileDesc);
		exit(1);
	}

	char inBuffer[2048];

	int ret = read(serverFileDesc, inBuffer, 2048);
	inBuffer[ret] = '\0';

	while (ret > 0)
	{
		printf("%s", inBuffer);

		ret = read(serverFileDesc, inBuffer, 2048);
		inBuffer[ret] = '\0';
	}

	close(serverFileDesc);

	return 0;
}