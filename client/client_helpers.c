/**
*	client_helpers provides functions to assist the main client in doing basic 
* 	tasks such as writing to the server and handling reading tasks.
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/


#include <stdlib.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <arpa/inet.h>

#include "client_helpers.h"

#include "../helpers/action_helpers.h"
#include "../helpers/string_helpers.h"
#include "../helpers/tcp_helpers.h"
#include "../helpers/validation_helpers.h"


/*
 * Gets the server address, whether it is in dotted notation or hostname
 */
void getHostAddress(const char *host, struct in_addr *address)
{
	if (isdigit(host[0]))
	{
		if (!inet_pton(AF_INET, host, address))
		{
			softExit("IP Address not valid");
		}
	}
	else
	{
		if (!getHostByName(host, address))
		{
			softExit("Hostname not found");
		}
	}
}

/**
 * handleServerRead
 * Given the server connection struct, handles reading from the socket and
 * performing any relevent tasks.
 * Returns a bool indicating wether the server is still readable from (i.e.
 * the socket is still open)
 */
bool handleServerRead(ServerConnection *server)
{
	char tempLine[MAX_STRING_ALLOC];
	int readCount, bufferSpaceLeft, readLineResult;
	bool stayConnected = false;

	tempLine[0] = '\0';
	bufferSpaceLeft = MAX_BUFFER - strlen(server->readBuffer) - 1;

	char tempBuffer[bufferSpaceLeft];
	tempBuffer[0] = '\0';

	readCount = Read(server->serverFileDesc, tempBuffer, bufferSpaceLeft);

	if (readCount > 0)
	{
		stayConnected = true;

		// Reterminate buffer
		tempBuffer[readCount] = '\0';

		// Add the newly read data to the server buffer
		strcat(server->readBuffer, tempBuffer);

		readLineResult = readLine(tempLine, server->readBuffer);

		while (readLineResult > 0)
		{
			printf("--- %s ---\n", tempLine);

			readLineResult = readLine(tempLine, server->readBuffer);
		}
	}

	return stayConnected;
}

/*
 * Given the server connection struct, attempts to write as much of the buffer
 * to the socket as possible, but only flushing at each new line character.
 * Returns a bool indicating wether the server is stillwritable to (i.e.
 * the socket is still open)
 */
bool handleServerWrite(ServerConnection *server)
{
	char tempLine[MAX_STRING_ALLOC];
	char tempBuffer[MAX_BUFFER];
	int readLineReturn, totalLength, writeReturn;
	bool stayConnected;

	tempBuffer[0] = '\0';

	readLineReturn = extractLineFromString(tempLine, server->writeBuffer);
	totalLength = strlen(tempBuffer) + strlen(tempLine);

	while ((readLineReturn > 0) && (totalLength < MAX_BUFFER))
	{
		// While there are more lines in the buffer and it is smaller than
		// the max buffer, keep adding to the temp buffer.
		strcat(tempBuffer, tempLine);

		readLineReturn = extractLineFromString(tempLine, server->writeBuffer);
		totalLength = strlen(server->writeBuffer) + strlen(tempLine);
	}

	// Flush the temp buffer.
	writeReturn = WriteString(server->serverFileDesc, tempBuffer);
	stayConnected = (writeReturn > 0);

	return stayConnected;
}

/*
 * Handles a read from stdin and any associated tasks including writing the 
 * results to the server
 */
bool handleStdinRead(ServerConnection *server)
{
	int totalLength;
	char lineBuffer[MAX_STRING_ALLOC];
	bool serverStillUp = true;

	fgets(lineBuffer, MAX_STRING, stdin);

	totalLength = strlen(server->writeBuffer) + strlen(lineBuffer);

	if (totalLength > MAX_BUFFER)
	{
		printf("The message you typed was too long, please shorten it and try again.\n");
	}
	else
	{
		if (validCommand(lineBuffer))
		{
			strcat(server->writeBuffer, lineBuffer);
			serverStillUp = handleServerWrite(server);
		}
	}

	return serverStillUp;
}

/*
 * Checks if the given string is a valid command that can be sent to the
 * server
 */
bool validCommand(char *command)
{
	bool isValid = false;
	printDebug("Checking commnand");

	/* if / else if statement for simplicity */

	/* Is not in form `JOIN <nickname> <hostname> <realname>` */
	if (startsWith(command, "JOIN") && wordCount(command) < 4)
	{
		printDebug("Invalid JOIN");
		printf("%s\n", INVALID_JOIN_COMMAND);
	}
	/* Is not in form `WHOIS <nickname>` */
	else if (startsWith(command, "WHOIS") && wordCount(command) != 2)
	{
		printDebug("Invalid WHOIS");
		printf("%s\n", INVALID_WHOIS_COMMAND);
	}
	/* Is not in form `MSG [<nickname>] <message>` */
	else if (startsWith(command, "MSG") && wordCount(command) < 2)
	{
		printDebug("Invalid MSG");
		printf("%s\n", INVALID_MSG_COMMAND);
	}
	/* Is a valid command */
	else if (startsWith(command, "JOIN") ||
			 startsWith(command, "WHOIS") ||
			 startsWith(command, "MSG") ||
			 startsWith(command, "TIME") ||
			 startsWith(command, "ALIVE") || 
			 startsWith(command, "QUIT"))
	{
		isValid = true;
	}
	/* Not a command we are aware of */
	else
	{
		printf("%s\n", UNKNOWN_COMMAND);
	}

	return isValid;
}