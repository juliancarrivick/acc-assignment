#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>

#include "mnc_client.h"
#include "client_helpers.h"

#include "../helpers/tcp_helpers.h"
#include "../helpers/action_helpers.h"

int main(int argc, char const *argv[])
{
	ServerConnection server;

	if (argc != 3)
	{
		fatalExit("Usage: ./mnc_client server_name server_port");
	}

	// Diable buffer on STDOUT, since we will be writing alot of text
	// that needs to be written to terminal immediately (eg ID>)
	setbuf(stdout, NULL);

	memset(&server, 0, sizeof(ServerConnection));

	server.serverName = argv[1];
	sscanf(argv[2], "%d", &server.serverPort);
	
	// Generate socket structure with address of '0', will be set when hostname
	// resolved.
	server.serverAddr = generateSocketAddress(0, server.serverPort);
	getHostAddress(server.serverName, &(server.serverAddr->sin_addr));

	server.serverFileDesc = tcpSocket();
	tcpConnect(server.serverFileDesc, server.serverAddr);

	printDebug("Entering Server Communication.");
	enterServerCommunication(&server);

	free(server.serverAddr);

	return 0;
}

/*
 * Enters communication with the server by using a select statement,
 * exiting when the server has closed hown or the client quits.
 */
void enterServerCommunication(ServerConnection *server)
{
	fd_set readFileDescriptors;
	bool stayConnected = true;

	FD_ZERO(&readFileDescriptors);

	printf("%d> ", STUDENT_ID);

	while (stayConnected)
	{
		FD_SET(server->serverFileDesc, &readFileDescriptors);
		FD_SET(STDIN_FILENO, &readFileDescriptors);

		Select(server->serverFileDesc + 1,
			&readFileDescriptors,
			NULL,
			NULL,
			NULL);
		
		if (FD_ISSET(STDIN_FILENO, &readFileDescriptors))
		{
			printDebug("Ready to read from STDIN");
			stayConnected = handleStdinRead(server);
			printf("%d> ", STUDENT_ID);
		}

		if (FD_ISSET(server->serverFileDesc, &readFileDescriptors))
		{
			printDebug("Ready to read from Server");
			stayConnected = handleServerRead(server);
		}
	}
}