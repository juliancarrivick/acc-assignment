/*
*	connection_pool provides functions and structs for managing a pool of
*	connections.
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#include <stdbool.h>
#include <stdlib.h>
#include <pthread.h>

#include "mnc_server.h"
#include "connection_pool.h"

#include "../helpers/unix_helpers.h"
#include "../helpers/action_helpers.h"


/*
 * Creates a new, empty pool for use
 */
MncConnectionPool *createConnectionPool(int maxSize, int timeout)
{
	printDebug("Creating new Connection Pool");

	MncConnectionPool *pool;

	pool = (MncConnectionPool*)calloc(1, sizeof(MncConnectionPool));

	pool->maxSize = maxSize;
	pool->currSize = 0;
	pool->connectionTimeout = timeout;

	// Don't need to intialise each connection to NULL since calloc does it 
	// for us. 
	pool->connections = (MncConnection**)calloc(maxSize, sizeof(MncConnection*));
	pool->connectionMutexs = (pthread_mutex_t*)calloc(maxSize, sizeof(pthread_mutex_t));

	// Initialise each mutex
	for (int i = 0; i < maxSize; ++i)
	{
		Pthread_mutex_init(&pool->connectionMutexs[i], NULL);
	}

	return pool;
}

/*
 * Adds a new connection to the pool
 */
int addToPool(MncConnectionPool *pool, MncConnection *newConnection)
{
	printDebug("Adding new connection to pool.");

	int returnVal = -1;
	int emptyIndex = -1;

	for (int i = 0; i < pool->maxSize; ++i)
	{
		// Make sure no one else steals our connection
		Pthread_mutex_lock(&pool->connectionMutexs[i]);

		if (pool->connections[i] == NULL)
		{
			emptyIndex = i;

			
			break; // Stop searching for empty index
		}

		// Everyone else is allowed to access the connection now.
		Pthread_mutex_unlock(&pool->connectionMutexs[i]);
	}

	if (emptyIndex != -1)
	{
		pool->connections[emptyIndex] = newConnection;
		pool->currSize++;

		// Add the mutex to the connection so that later on, it can be used
		// without the need for the pool
		newConnection->connectionMutex = &pool->connectionMutexs[emptyIndex];

		// Everyone else is allowed to access the connection now.
		Pthread_mutex_unlock(&pool->connectionMutexs[emptyIndex]);

		returnVal = 0; // Successful addition
	}
	else 
	{
		returnVal = E_POOLFULL;
	}
	
	return returnVal;
}

/*
 * Creates a brand new connection, then adds it to the pool
 */
int createNewConnectionAndAddToPool(MncConnectionPool *pool)
{
	return addToPool(pool, generateConnectionStructure());
}


/*
 * Removes a given connection from the pool
 */
int removeFromPool(MncConnectionPool *pool, MncConnection *oldConnection)
{
	printDebug("Removing connection from pool");

	int returnValue = -1;
	int connectionIndex = -1;

	for (int i = 0; i < pool->maxSize; ++i)
	{
		// Make sure no one else uses the connection
		Pthread_mutex_lock(&pool->connectionMutexs[i]);

		/** Deliberately comparing the pointer values here 
		  *	Much faster than comparing each of the values in both the structs.
		  */
		if (pool->connections[i] == oldConnection)
		{
			connectionIndex = i;


			break; // We have finished searching
		}

		// Everyone else is allowed to access the connection now.
		Pthread_mutex_unlock(&pool->connectionMutexs[i]);
	}

	if (connectionIndex != -1)
	{		
		pool->connections[connectionIndex] = NULL;
		pool->currSize--;

		// Everyone else can use this connection slot now.
		Pthread_mutex_unlock(&pool->connectionMutexs[connectionIndex]);

		returnValue = 0; // Successful removal.
	}

	return returnValue;
}

/*
 * Returns the current size of the pool
 */
int getCurrentSizeOfPool(MncConnectionPool *pool)
{
	return pool->currSize;
}

/*
 * Self explainitary
 */
bool isPoolFull(MncConnectionPool *pool)
{
	return pool->currSize == pool->maxSize;
}

/*
 * Frees all pool memory
 */
void freePool(MncConnectionPool *pool)
{
	printDebug("Freeing whole pool");

	for (int i = 0; i < pool->maxSize; ++i)
	{
		Pthread_mutex_destroy(&pool->connectionMutexs[i]);
	}

	freeConnectionArray(pool->connections, pool->maxSize);
	free(pool);
}