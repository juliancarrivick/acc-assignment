/**
*	command handler deals with all the command to action handling
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#include <string.h>
#include <time.h>
#include <stdio.h>

#include "command_handler.h"
#include "pool_helpers.h"
#include "server_config.h"
#include "mnc_connection_helpers.h"
#include "connection_handler.h"

#include "../helpers/string_helpers.h"
#include "../helpers/action_helpers.h"
#include "../helpers/tcp_helpers.h"
#include "../helpers/unix_helpers.h"
#include "../helpers/validation_helpers.h"

/*
 * Static Declaration of the command -> handler mappings
 */
static const MncCommand MNC_COMMANDS[] =
	{
		{
			JOIN,
			"JOIN",
			handleJoinCommand
		},
		{
			WHOIS,
			"WHOIS",
			handleWhoIsCommand
		},
		{
			MESSAGE,
			"MSG",
			handleMsgCommand
		},
		{
			TIME,
			"TIME",
			handleTimeCommand
		},
		{
			ALIVE,
			"ALIVE",
			handleAliveCommand
		},
		{
			QUIT,
			"QUIT",
			handleQuitCommand
		}
	};

/*
 * Static count on the number of commands available
 */
static const int NUM_MNC_COMMANDS = 6;

/*
 * Function to take a raw command, detect which command it is, seperate the
 * data and call the correct handler
 */
ProcessorReturnCode parseAndProcessCommand(MncConnectionPool *pool,
										   MncConnection *connection, 
										   char *command)
{
	bool handledCommand = false;
	HandlerReturnCode handleReturn = HANDLER_OK;
	ProcessorReturnCode result = PROCESSOR_OK;
	char commandData[MAX_STRING];

	trimLeadingAndTrailingWhitespace(command);

	// Check each available command again the given command. 
	for (int i = 0; i < NUM_MNC_COMMANDS; ++i)
	{
		if (startsWith(command, MNC_COMMANDS[i].commandString))
		{
			if (!connection->hasJoined && MNC_COMMANDS[i].command != JOIN)
			{
				printDebug("Client is trying to run commands when not joined.");

				messageFromServerToUser(connection, INVALID_JOIN_COMMAND);
			}
			else if (connection->hasJoined && MNC_COMMANDS[i].command == JOIN)
			{
				printDebug("Client is trying to join when they are already joined.");

				messageFromServerToUser(connection, ALREADY_JOINED);
			}
			else
			{
				// Remove the command from the data and just pass the rest of it 
				// to the handler.
				removeSubString(commandData, command, 
								strlen(MNC_COMMANDS[i].commandString),
								strlen(command));

				trimLeadingAndTrailingWhitespace(commandData);

				// Handle the command.
				handleReturn = MNC_COMMANDS[i].handler(pool, connection, commandData);

			}
			
			handledCommand = true;
			break;
		}
	}

	if (!handledCommand)
	{
		printDebug("Command not found");

		messageFromServerToUser(connection, UNKNOWN_COMMAND);
	}

	if (handleReturn == HANDLER_QUIT)
	{
		result = PROCESSOR_QUIT;
	}

	return result;
}

/*******************
 * HANDLER METHODS *
 *******************/

HandlerReturnCode handleJoinCommand(MncConnectionPool *pool, 
									MncConnection *connection, 
									char *joinData)
{
	printDebug("JOIN Command Received.");

	bool isJoinDataValid, nickNameExists, realNameExists;
	HandlerReturnCode returnValue = HANDLER_OK;
	char serverMessage[MAX_STRING_ALLOC];

	isJoinDataValid = validateConnectionJoinMessage(connection, joinData);

	if (isJoinDataValid)
	{
		printDebug("JOIN Data is valid.");

		/* Even if data is valid, we need to make sure the nicknamd and 
			realname don't already exist */
		realNameExists = (getConnectionByRealName(pool, connection->realName) != NULL);
		nickNameExists = (getConnectionByNickName(pool, connection->nickName) != NULL);

		if (realNameExists)
		{
			printDebug("Realname already exists.");
			messageFromServerToUser(connection, REALNAME_EXISTS);
		}
		else if (nickNameExists)
		{
				printDebug("Nickname already exists");

				messageFromServerToUser(connection, NICKNAME_EXISTS);
		}
		else
		{
			if (isPoolFull(pool))
			{
				printDebug("Connection Pool is full. Closing Connection");
				messageFromServerToUser(connection, POOL_FULL);
				returnValue = HANDLER_QUIT;
			}
			else
			{
				printDebug("JOIN has been confirmed, welcoming.");
					
				/* If all OK, set the time joined and add to the pool */
				time(&connection->timeJoined);
				connection->hasJoined = true;
				addToPool(pool, connection);

				messageFromServerToUser(connection, WELCOME_MESSAGE);
				sprintf(serverMessage, "%s %s", PARTIAL_GROUP_WELCOME_MESSAGE, 
						connection->nickName);
				messageFromServerOnBehalfOfUser(pool, connection, serverMessage);
			}
		}
	}

	return returnValue;
}

HandlerReturnCode handleWhoIsCommand(MncConnectionPool *pool,
									 MncConnection *connection, 
									 char *whoIsData)
{
	printDebug("WHOIS Command Received.");

	char tempMessage[MAX_STRING_ALLOC];
	char tempTimeString[MAX_STRING_ALLOC];

	MncConnection *whoIsConnection = getConnectionByNickName(pool, whoIsData);

	if (whoIsConnection == NULL)
	{
		messageFromServerToUser(connection, INVALID_WHOIS_COMMAND);
	}
	else
	{
		printDateTime(tempTimeString, &whoIsConnection->timeJoined);

		// Format message and send to origin
		sprintf(tempMessage, 
			"%s is:\nRealname: %s\nHostname: %s\nTime Joined: %s",
			whoIsConnection->nickName,
			whoIsConnection->realName,
			whoIsConnection->hostName,
			tempTimeString);

		messageFromServerToUser(connection, tempMessage);
	}

	return HANDLER_OK;
}

HandlerReturnCode handleMsgCommand(MncConnectionPool *pool,
								   MncConnection *connection, 
								   char *msgData)
{	
	printDebug("MSG Command Received.");

	char tempTargetStr[MAX_STRING_ALLOC], 
		 tempMessageStr[MAX_STRING_ALLOC],
		 scanfString[MAX_STRING_ALLOC];
	int wordsRead;
	bool isValidMessage = false, toSpecificUser = false;
	MncConnection *targetUser;

	sprintf(scanfString, "%%%ds %%%d", MAX_STRING, MAX_STRING);
	strcat(scanfString, "[^\t\n]");

	wordsRead = sscanf(msgData, scanfString, tempTargetStr, tempMessageStr);

	if (wordsRead > 2)
	{
		messageFromServerToUser(connection, INVALID_MSG_COMMAND);
	}
	else if (wordsRead == 1)
	{
		isValidMessage = validMessage(msgData);

		if (isValidMessage)
		{
			messageFromUser(pool, connection, msgData);
		}
	}
	else
	{
		trimLeadingAndTrailingWhitespace(tempTargetStr);
		targetUser = getConnectionByNickName(pool, tempTargetStr);
		toSpecificUser = (targetUser != NULL);

		if (toSpecificUser)
		{
			printDebug("Sending message to specific user");
			isValidMessage = validMessage(tempMessageStr);

			if (isValidMessage)
			{
				messageToUser(targetUser, connection->nickName, tempMessageStr);
			}
		}
		else
		{
			printDebug("Sending message to all users");
			isValidMessage = validMessage(msgData);

			if (isValidMessage)
			{
				messageFromUser(pool, connection, msgData);
			}
		}

		if (!isValidMessage)
		{
			messageFromServerToUser(connection, INVALID_MESSAGE);
		}
	}

	return HANDLER_OK;
}

HandlerReturnCode handleTimeCommand(MncConnectionPool *pool,
									MncConnection *connection,
									char *timeData)
{
	printDebug("TIME Command Received.");

	char timeStr[MAX_STRING];
	time_t unixTime;

	// Get the date/time and write it to an ASCII string.
	unixTime = time(NULL);
	printDateTime(timeStr, &unixTime);

	messageFromServerToUser(connection, timeStr);

	return HANDLER_OK;
}

HandlerReturnCode handleAliveCommand(MncConnectionPool *pool, 
									 MncConnection *connection, 
									 char *aliveCommand)
{
	printDebug("ALIVE Command Received.");

	// Do nothing, the mere presence of this command resets the connection timeout

	return HANDLER_OK;
}

HandlerReturnCode handleQuitCommand(MncConnectionPool *pool,
									MncConnection *connection,
									char *quitMsg)
{
	printDebug("QUIT Command Received.");

	double connectionTime;
	time_t timeNow;
	char goodbyeMessage[MAX_STRING_ALLOC], serverMessage[MAX_STRING_ALLOC];

	timeNow = time(NULL);

	connectionTime = difftime(timeNow, connection->timeJoined);
	sprintf(goodbyeMessage, 
		"Thanks for being with us for the past %.0lf seconds. Bye %s!",
		connectionTime,
		connection->nickName);

	messageFromServerToUser(connection, goodbyeMessage);

	if (strlen(quitMsg) > 0)
	{
		messageFromUser(pool, connection, quitMsg);
	}

	sprintf(serverMessage, "%s %s", connection->nickName, PARTIAL_DISCONNECTED_CLIENT);
	messageFromServerOnBehalfOfUser(pool, connection, serverMessage);

	Shutdown(connection->fileDesc, SHUT_RDWR);

	return HANDLER_QUIT;
}