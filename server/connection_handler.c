/**
*	connection handler deals with taking a connection and delegating the read
* 	tasks
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <stdlib.h>

#include "mnc_connection.h"
#include "connection_handler.h"
#include "connection_pool.h"
#include "server_config.h"
#include "command_handler.h"

#include "../helpers/tcp_helpers.h"
#include "../helpers/string_helpers.h"
#include "../helpers/action_helpers.h"
#include "../helpers/unix_helpers.h"

/*
 * Deals with reading from the client, and telling the callback that there is 
 * data ready to read
 */
int enterClientCommunication(MncConnectionPool *pool, MncConnection *connection, 
							 ReadHandler readCallback)
{
	fd_set readFileDescriptors;
	struct timeval timeout;
	int returnValue = 1, fdsReady;

	FD_ZERO(&readFileDescriptors);

	timeout = generateTimeStruct(pool->connectionTimeout, 0);

	while (returnValue > 0)
	{
		FD_SET(connection->fileDesc, &readFileDescriptors);

		fdsReady = Select(connection->fileDesc + 1,
			&readFileDescriptors,
			NULL,
			NULL,
			&timeout);

		
		if (fdsReady > 0)
		{
			if (FD_ISSET(connection->fileDesc, &readFileDescriptors))
			{
				printDebug("Ready to read from socket");
				returnValue = readCallback(pool, connection);
			}
		}
		else
		{
			printDebug("Connection timed out, exiting.");
			WriteLine(connection->fileDesc, CONNECTION_TIMEDOUT);
			returnValue = -1;
		}
	}

	return returnValue;
}

/*
 * Handles a general read, reads as much data as the connection buffer can 
 * handle, then line by line gives it to the parseAndProcessCommand function
 * to deal with
 */
int handleRead(MncConnectionPool *pool, MncConnection *connection)
{
	char tempLine[MAX_STRING_ALLOC];
	int readLineResult;
	int bufferSpaceLeft, readCount, returnValue = 1;

	bufferSpaceLeft = MAX_BUFFER - strlen(connection->buffer) - 1;

	char tempBuffer[bufferSpaceLeft];

	readCount = Read(connection->fileDesc, tempBuffer, bufferSpaceLeft);

	if (readCount > 0)
	{
		// Add null terminator
		tempBuffer[readCount] = '\0';

		// Add the newly read data to the connection buffer
		strcat(connection->buffer, tempBuffer);

		readLineResult = readLine(tempLine, connection->buffer);

		while (readLineResult > 0)
		{
			printDebug("Line Read from client.");

			// Process command
			if (parseAndProcessCommand(pool, connection, tempLine) == PROCESSOR_QUIT)
			{
				printDebug("Recieved quit command, exiting.");
				readLineResult = 0;
				returnValue = -1;
			}
			else
			{
				readLineResult = readLine(tempLine, connection->buffer);
			}
		}
	}

	if (readCount <= 0)
	{
		returnValue = -1;
	}

	return returnValue;
}