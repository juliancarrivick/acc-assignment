/**
*	pool_helpers provides functions for pool related functions, mostly
*	sending messages to clients
*	connection
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef POOL_HELPERS_H
#define POOL_HELPERS_H

#include <stdbool.h>

#include "connection_pool.h"
#include "mnc_connection.h"

/*
 * Retrieve a connection by nickname, realname or hostname
 */
MncConnection *getConnectionByNickName(const MncConnectionPool *pool, char *nickName);
MncConnection *getConnectionByRealName(const MncConnectionPool *pool, char *realName);
MncConnection *getConnectionByHostName(const MncConnectionPool *pool, char *hostName);

/*
 * Send messages to a user, from other users or the server
 */
int messageToUser(MncConnection *connection, const char *fromName, char *message);
int messageFromUser(const MncConnectionPool *pool, MncConnection *connection, char *message);
int messageFromServer(const MncConnectionPool *pool, char *message);
int messageFromServerToUser(MncConnection *connection, char *message);
int messageFromServerOnBehalfOfUser(const MncConnectionPool *pool, MncConnection *userConnection, char *message);

/*
 * Send a message to all users
 */
int notifyPool(const MncConnectionPool *pool, char *message);

#endif
