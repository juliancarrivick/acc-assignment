/**
*	mnc_connection_helpers provides functions for running login on a connection
*	connection
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#include <stdio.h>
#include <string.h>

#include "mnc_connection_helpers.h"
#include "pool_helpers.h"
#include "server_config.h"

#include "../helpers/validation_helpers.h"
#include "../helpers/tcp_helpers.h"
#include "../helpers/string_helpers.h"
#include "../helpers/action_helpers.h"

/*
 * Validates a connection's join message
 */
bool validateConnectionJoinMessage(MncConnection *connection, char *joinData)
{
	bool dataValid = true;
	char nickName[MAX_STRING_ALLOC], 
		 hostName[MAX_STRING_ALLOC],
		 realName[MAX_STRING_ALLOC],
		 scanfString[MAX_STRING_ALLOC];

	int wordsRead = -1;

	/* Dynamically create scanf format string to prevent buffer overflow */
	sprintf(scanfString, "%%%ds %%%ds %%%d", MAX_STRING, MAX_STRING, MAX_STRING);
	strcat(scanfString, "[^\t\n]");

	wordsRead = sscanf(joinData, scanfString, nickName, hostName, realName);

	// Ensure read data is valid
	if (wordsRead != 3)
	{
		messageFromServerToUser(connection, INVALID_JOIN_COMMAND);
		dataValid = false;
	}
	else
	{
		if (!validNickName(nickName))
		{
			messageFromServerToUser(connection, INVALID_NICKNAME);
			dataValid = false;
		}
		if (!validHostName(hostName))
		{
			messageFromServerToUser(connection, INVALID_HOSTNAME);
			dataValid = false;
		}
		if (!validFullName(realName))
		{
			messageFromServerToUser(connection, INVALID_REALNAME);
			dataValid = false;
		}
	}

	if (dataValid)
	{
		strcpy(connection->nickName, nickName);
		strcpy(connection->hostName, hostName);
		strcpy(connection->realName, realName);
	}

	return dataValid;
}

/*
 * A readHandler (see connection_handler.h), handles a client rechoosing
 * its nickname
 */
int handleNickNameRePicking(MncConnectionPool *pool, MncConnection *connection)
{
	char tempLine[MAX_STRING_ALLOC];
	int readLineResult;
	int bufferSpaceLeft, readCount, returnValue = 1;
	bool nickNameOk = false;

	bufferSpaceLeft = MAX_BUFFER - strlen(connection->buffer) - 1;

	char tempBuffer[bufferSpaceLeft];

	readCount = Read(connection->fileDesc, tempBuffer, bufferSpaceLeft);

	if (readCount > 0)
	{
		// Add the newly read data to the connection buffer
		strcat(connection->buffer, tempBuffer);

		readLineResult = readLine(tempLine, connection->buffer);

		while (readLineResult > 0 && !nickNameOk)
		{
			printDebug("Nickname Read from client.");

			nickNameOk = checkNickName(pool, connection, tempLine);

			if (nickNameOk)
			{
				returnValue = 0;
			}

			readLineResult = readLine(tempLine, connection->buffer);
		}
	}
	else
	{
		returnValue = -1;
	}


	return returnValue;
}

/*
 * Checks that a nickname is valid or not
 */
bool checkNickName(MncConnectionPool *pool, MncConnection *connection, char *nickName)
{
	bool nickNameExists, nickNameOk;

	nickNameOk = false;

	trimLeadingAndTrailingWhitespace(nickName);
	nickNameExists = (getConnectionByNickName(pool, nickName) != NULL);

	if (nickNameExists)
	{
		messageFromServerToUser(connection, NICKNAME_EXISTS);
	}
	else
	{
		printDebug("Entered nickname is available.");

		if (validNickName(nickName))
		{
			strcpy(connection->nickName, nickName);
			nickNameOk = true;
		}
		else
		{
			printDebug("Entered nickname is not valid.");
			messageFromServerToUser(connection, INVALID_NICKNAME);
		}
	}

	return nickNameOk;
}