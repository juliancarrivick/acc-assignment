/**
*	mnc_server provides is the main file for the server, it sets up the 
*	listening socket and creates a new thread for each client.
*	connection
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef SERVER_H
#define SERVER_H




#include "mnc_connection.h"



/*
 * When there is a new connection, handle it for me
 */
void handleNewConnection(MncConnection *newConnection);

/*
 * Function to be run when a new thread is created
 */
void *newConnectionThreadStart(void *data);

/*
 * Process the servers arguments
 */
void processArguments(int argc, char **argv);




#endif