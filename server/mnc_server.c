/**
*	mnc_server provides is the main file for the server, it sets up the 
*	listening socket and creates a new thread for each client.
*	connection
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

/* System Headers */
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <stdbool.h>
#include <sys/uio.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>

/* This module's headers */
#include "mnc_server.h"
#include "server_config.h"
#include "connection_pool.h"
#include "mnc_connection.h"
#include "connection_handler.h"
#include "pool_helpers.h"

/* The helper headers */
#include "../helpers/tcp_helpers.h"
#include "../helpers/unix_helpers.h"
#include "../helpers/action_helpers.h"
#include "../helpers/validation_helpers.h"


/* 
 * Global pool for use by helper functions.
 * Is static so that only this file can access it.
 */
static MncConnectionPool *connectionPool;
/*
 * Global config values 
 */
static int maxIdleTime, maxNoClients;
static bool finishServing;


int main(int argc, char** argv)
{
	int listenFileDescriptor, reuseAddressOption;
	struct sockaddr_in *serverAddress;
	MncConnection *tempConnection;

	processArguments(argc, argv);

	finishServing = false;
	reuseAddressOption = true;

	connectionPool = createConnectionPool(maxNoClients, maxIdleTime);

	/* Setup listening port */
	serverAddress = generateSocketAddress(INADDR_ANY, SERVER_LISTEN_PORT);
	listenFileDescriptor = tcpSocket();
	SetSockOpt(listenFileDescriptor, SOL_SOCKET, SO_REUSEADDR, &reuseAddressOption, sizeof(int));
	tcpListen(SERVER_LISTEN_QUEUE, listenFileDescriptor, serverAddress);

	while(!finishServing)
	{
		tempConnection = generateConnectionStructure();

		printDebug("Waiting to accept connection");
		tempConnection->fileDesc = tcpAccept(
			listenFileDescriptor,
			&(tempConnection->socketAddress),
			&(tempConnection->sockAddressLength));

		if (tempConnection->fileDesc < 0)
		{
			printDebug("Could not connect to new client. Cleaning up and continuing");

			// Cleanup
			Close(tempConnection->fileDesc);
			freeConnectionStructure(tempConnection);
		}
		else
		{
			handleNewConnection(tempConnection);	
		}
	}

	printDebug("Finished Serving, cleaning up.");

	free (serverAddress);
	freePool(connectionPool);
	return 0;
}

/*
 * Process the servers arguments
 */
void processArguments(int argc, char **argv)
{
	if (argc != 3)
	{
		softExit("Usage: ./mnc_server maxNoClients maxIdleTime");
	}

	// If max no clients not integer or not valid
	if (sscanf(argv[1], "%d", &maxNoClients) != 1 || !validMaxNoClients(maxNoClients))
	{
		softExit("Max number of clients: 1 <= max <= 10");
	}

	if (sscanf(argv[2], "%d", &maxIdleTime) != 1 || !validTimeout(maxIdleTime))
	{
		softExit("Max Idle Time: 1 <= idle time <= 120");
	}
}

/*
 * When there is a new connection, handle it for me
 */
void handleNewConnection(MncConnection *newConnection)
{
	if (isPoolFull(connectionPool))
	{
		printDebug("Connection Pool is full. Closing Connection");
		messageFromServerToUser(newConnection, POOL_FULL);
		close(newConnection->fileDesc);
	}
	else
	{
		/* 
		 * Spawn new thread specifically for this connection
		 * It will be entrusted with removing it from the pool and freeing 
		 * all memory when the connection is closed.
		 * Don't add the connection to the pool yet, only add it once the JOIN
		 * command has been correctly entered.
		 */
		printDebug("Creating new thread to handle new client connection.");

		Pthread_create(
			&(newConnection->threadId),
			NULL,
			newConnectionThreadStart,
			newConnection);
	}
}

/*
 * Function to be run when a new thread is created
 */
void *newConnectionThreadStart(void *data)
{
	printDebug("Entered new thread, detaching thread");

	MncConnection *clientConnection = (MncConnection*)data;

	/* So main() doesn't have to wait for execution to complete */
	pthread_detach(pthread_self());

	/* Initialise connection values */
	time(&clientConnection->timeJoined);
	time(&clientConnection->lastActivityTime);

	enterClientCommunication(connectionPool, clientConnection, handleRead);

	printDebug("Communication with client finished.");

	/* Cleanup - clientConnection is free'd when removed from the pool */
	Close(clientConnection->fileDesc);
	removeFromPool(connectionPool, clientConnection);
	freeConnectionStructure(clientConnection);

	return NULL;
}