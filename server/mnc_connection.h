/**
*	mnc_connection provides functions and structs for a single client connection
*	connection
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef MNC_CONNECTION_H
#define MNC_CONNECTION_H

#include <netinet/in.h>
#include <time.h>
#include <pthread.h>
#include <stdbool.h>

#include "../helpers/global_config.h"

/****************
*	DATA TYPES	*
****************/

/**
*	A struct which represents a single connection to a client.
*/
typedef struct
{
	char nickName[MAX_NICKNAME_ALLOC];
	char realName[MAX_REALNAME_ALLOC];
	char hostName[MAX_HOSTNAME_ALLOC];
	time_t timeJoined, lastActivityTime;
	bool hasJoined; // true if in the connection pool, false otherwise.
	struct sockaddr_in socketAddress;
	socklen_t sockAddressLength;
	int fileDesc;
	pthread_t threadId;
	pthread_mutex_t *connectionMutex; // Pointer to a mutex, is set upon joining the poool
	char buffer[MAX_BUFFER];
} MncConnection;




/****************
*	FUNCTIONS 	*
****************/

/*
 * Generates the given number of connections, returning the array
 */
MncConnection **initialiseConnectionStructures(int number);

/*
 * Creates a single connection
 */
MncConnection *generateConnectionStructure();

/*
 * Frees an entire array's memory
 */
void freeConnectionArray(MncConnection **connections, int number);

/*
 * Frees a single connection's memory
 */
void freeConnectionStructure(MncConnection *connection);

#endif