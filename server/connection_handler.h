/**
*	connection handler deals with taking a connection and delegating the read
* 	tasks
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef CONNECTION_HANDLER_H
#define CONNECTION_HANDLER_H

#include <sys/time.h>

#include "mnc_connection.h"
#include "connection_pool.h"

/*
 * Function pointer for Read handling. enterClientCommunication will call this
 * function type when there is data ready to be read from the client
 * enterClientCommunication will keep reading from the client as long as this 
 * function returns > 0. If it returns 0 then it is considered that the socket is
 * still open, but we are finished this section of reading from the client
 * If <0 is returned, enterClientCommunication assumes the socket is closed
 * or there has been an error and also returns.
 */
typedef int (*ReadHandler)(MncConnectionPool *, MncConnection *);

/*
 * Deals with reading from the client, and telling the callback that there is 
 * data ready to read
 */
int enterClientCommunication(MncConnectionPool *pool, MncConnection *connection,
							 ReadHandler readCallback);

/*
 * Handles a general read, reads as much data as the connection buffer can 
 * handle, then line by line gives it to the parseAndProcessCommand function
 * to deal with
 */
int handleRead(MncConnectionPool *pool, MncConnection *connection);


#endif