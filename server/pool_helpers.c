/**
*	pool_helpers provides functions for pool related functions, mostly
*	sending messages to clients
*	connection
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#include <string.h>

#include <stdio.h>

#include "pool_helpers.h"
#include "server_config.h"

#include "../helpers/unix_helpers.h"
#include "../helpers/tcp_helpers.h"

/*
 * Retrieve a connection by nickname
 */
MncConnection *getConnectionByNickName(const MncConnectionPool *pool, char *nickName)
{
	for (int i = 0; i < pool->maxSize; ++i)
	{
		if (pool->connections[i] != NULL &&
			strcmp(pool->connections[i]->nickName, nickName) == 0)
		{
			return pool->connections[i];
		}
	}

	return NULL;
}

/*
 * Retrieve a connection by realname
 */
MncConnection *getConnectionByRealName(const MncConnectionPool *pool, char *realName)
{
	for (int i = 0; i < pool->maxSize; ++i)
	{
		if (pool->connections[i] != NULL &&
			strcmp(pool->connections[i]->realName, realName) == 0)
		{
			return pool->connections[i];
		}
	}

	return NULL;
}

/*
 * Retrieve a connection by hostname
 */
MncConnection *getConnectionByHostName(const MncConnectionPool *pool, char *hostName)
{
	for (int i = 0; i < pool->maxSize; ++i)
	{
		if (pool->connections[i] != NULL &&
			strcmp(pool->connections[i]->hostName, hostName) == 0)
		{
			return pool->connections[i];
		}
	}

	return NULL;
}

/*
 * Send a message to all users
 */
int notifyPool(const MncConnectionPool *pool, char *message)
{
	for (int i = 0; i < pool->maxSize; ++i)
	{
		// Make sure no one else can access our connection
		Pthread_mutex_lock(&pool->connectionMutexs[i]);

		if (pool->connections[i] != NULL)
		{
			WriteLine(pool->connections[i]->fileDesc, message);
		}

		// Everyone else can use this connection slot now.
		Pthread_mutex_unlock(&pool->connectionMutexs[i]);
	}

	return 0;
}

/*
 * Send a message to the pool from a given user
 */
int messageFromUser(const MncConnectionPool *pool, MncConnection *connection, char *message)
{
	char tempMessage[MAX_STRING_ALLOC];

	for (int i = 0; i < pool->maxSize; ++i)
	{
		// Make sure no one else can access our connection
		Pthread_mutex_lock(&pool->connectionMutexs[i]);

		if (pool->connections[i] != NULL && pool->connections[i] != connection)
		{
			sprintf(tempMessage, "%s: %s", connection->nickName, message);
			WriteLine(pool->connections[i]->fileDesc, tempMessage);	
		}
	
		// Everyone else can use this connection slot now.
		Pthread_mutex_unlock(&pool->connectionMutexs[i]);
	}

	return 0;
}

/*
 * Send a message to a user with the given from name
 */
int messageToUser(MncConnection *connection, const char *fromName, char *message)
{
	char tempMessage[MAX_STRING_ALLOC];

	Pthread_mutex_lock(connection->connectionMutex);

	sprintf(tempMessage, "%s: %s", fromName, message);
	WriteLine(connection->fileDesc, tempMessage);

	Pthread_mutex_unlock(connection->connectionMutex);

	return 0;
}

/*
 * Send a message from the server to the user 
 */
int messageFromServerToUser(MncConnection *connection, char *message)
{
	messageToUser(connection, SERVER_NAME, message);

	return 0;
}

/*
 * Send a message from the server to everyone
 */
int messageFromServer(const MncConnectionPool *pool, char *message)
{
	char tempMessage[MAX_STRING_ALLOC];

	sprintf(tempMessage, "%s: %s", SERVER_NAME, message);

	notifyPool(pool, tempMessage);

	return 0;
}

/*
 * Send a message from a given user to everyone
 */
int messageFromServerOnBehalfOfUser(const MncConnectionPool *pool, MncConnection *userConnection, char *message)
{
	for (int i = 0; i < pool->maxSize; ++i)
	{
		if (pool->connections[i] != NULL && pool->connections[i] != userConnection)
		{
			messageFromServerToUser(pool->connections[i], message);
		}
	}

	return 0;
}