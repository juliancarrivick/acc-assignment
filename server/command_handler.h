/**
*	command handler deals with all the command to action handling
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef COMMAND_HANDLER_H
#define COMMAND_HANDLER_H

#include "mnc_connection.h"
#include "connection_pool.h"

/*
 * HANDLER RETURN CODES
 */
typedef enum {HANDLER_OK, HANDLER_QUIT} HandlerReturnCode;

/*
 * PROCESSOR RETURN CODES
 */
typedef enum {PROCESSOR_OK, PROCESSOR_JOINED, PROCESSOR_QUIT} ProcessorReturnCode;

/*
 * Enum representing the different types of commands that can be executed
 */
typedef enum {JOIN, WHOIS, MESSAGE, TIME, ALIVE, QUIT} MncCommandEnum;

/*
 * Defines a function pointer for a generic command handler
 * Takes in a connection pool, connection, and string of data.
 * Returns a handler code which determines whether to keep reading or to quit.
 */
typedef HandlerReturnCode (*commandHandler)(MncConnectionPool *, MncConnection *, char *);

/*
 * Struct defining a single command, holds the command enum, a string
 * representing that command, and a handler 
 */
typedef struct {
	MncCommandEnum command;
	const char *commandString;
	commandHandler handler;
} MncCommand;


/*
 * Defines each of the handlers available
 * Technicallt, timeHandler and aliveHandler do not need data, but added so
 * that we can use the function pointer.
 */
HandlerReturnCode handleJoinCommand(MncConnectionPool *pool, 
									MncConnection *connection,
									char *joinData);

HandlerReturnCode handleWhoIsCommand(MncConnectionPool *pool,
									 MncConnection *connection,
									 char *whoIsData);

HandlerReturnCode handleMsgCommand(MncConnectionPool *pool,
								   MncConnection *connection,
								   char *msgData);

HandlerReturnCode handleTimeCommand(MncConnectionPool *pool,
									MncConnection *connection, 
									char *timeData);

HandlerReturnCode handleAliveCommand(MncConnectionPool *pool,
									 MncConnection *connection, 
									 char *alive);

HandlerReturnCode handleQuitCommand(MncConnectionPool *pool,
									MncConnection *connection,
									char *quitMsg);

/*
 * Function to take a raw command, detect which command it is, seperate the
 * data and call the correct handler
 */
ProcessorReturnCode parseAndProcessCommand(MncConnectionPool *pool,
										   MncConnection *connection,
										   char *command);

#endif