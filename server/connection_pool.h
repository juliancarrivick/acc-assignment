/*
*	connection_pool provides functions and structs for managing a pool of
*	connections.
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef CONNECTION_POOL_H
#define CONNECTION_POOL_H 

#include <stdbool.h>

#include "mnc_connection.h"




/*************
* DATA TYPES *
**************/

/**
  * Struct Representing a a pool of client connections up to a maximum size
  * 	- connections: An Array of MncConnections up to maxSize
  *		- connectionMutexs: An array of mutexs that correspond with the connections
  			Need a seperate array rather than just putting in the connection
  			array as they need to persist as connections come and go.
  */
typedef struct
{
	MncConnection **connections;
	pthread_mutex_t *connectionMutexs;
	int maxSize, currSize, connectionTimeout;
} MncConnectionPool;

/****************
* RETURN VALUES *
*****************/
#define E_POOLFULL -1



/************
* FUNCTIONS *
*************/

/*
 * Creates a new, empty pool for use
 */
MncConnectionPool *createConnectionPool(int maxSize, int timeout);

/*
 * Adds a new connection to the pool
 */
int addToPool(MncConnectionPool *pool, MncConnection *newConnection);

/*
 * Creates a brand new connection, then adds it to the pool
 */
int createNewConnectionAndAddToPool(MncConnectionPool *pool);

/*
 * Removes a given connection from the pool
 */
int removeFromPool(MncConnectionPool *pool, MncConnection *oldConnection);

/*
 * Returns the current size of the pool
 */
int getCurrentSizeOfPool(MncConnectionPool *pool);

/*
 * Self explainitary
 */
bool isPoolFull(MncConnectionPool *pool);

/*
 * Frees all pool memory
 */
void freePool(MncConnectionPool *pool);

#endif