/**
*	mnc_connection provides functions and structs for a single client connection
*	connection
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/


#include <stdlib.h>

#include "mnc_connection.h"
#include "../helpers/unix_helpers.h"



/*
 * Generates the given number of connections, returning the array
 */
MncConnection **initialiseConnectionStructures(int number)
{
	MncConnection **connections;
	connections = (MncConnection**)malloc(number * sizeof(MncConnection*));

	for (int i = 0; i < number; ++i)
	{
		connections[i] = NULL;
	}

	return connections;
}

/*
 * Creates a single connection
 */
MncConnection *generateConnectionStructure()
{
	// Zero out allocated memory
	MncConnection *connection = (MncConnection*)calloc(1, sizeof(MncConnection));

	return connection;
}

/*
 * Frees an entire array's memory
 */
void freeConnectionArray(MncConnection **connections, int number)
{
	for (int i = 0; i < number; ++i)
	{
		if (connections[1] != NULL)
		{
			freeConnectionStructure(connections[i]);
		}
	}

	free(connections);
}

/*
 * Frees a single connection's memory
 */
void freeConnectionStructure(MncConnection *connection)
{
	free(connection);
}