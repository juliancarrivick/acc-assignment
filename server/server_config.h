/**
*	server_config provides server specific configuration values
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef SERVER_CONFIG_H
#define SERVER_CONFIG_H

/* The port to listen on */
#define SERVER_LISTEN_PORT 52010

/* The listen queue number */
#define SERVER_LISTEN_QUEUE 10

/******************
 * MESSAGE MACROS *
 ******************/
#define SERVER_NAME "Server"

#define WELCOME_MESSAGE "Welcome to MNC!"

#define ALREADY_JOINED "You have already joined the chat room"
#define CONNECTION_TIMEDOUT "Your connection has timed out, please reconnect"
#define POOL_FULL "There are no available connections right now"

#define NICKNAME_EXISTS "That nickname exists already, please rejoin with a different one"
#define REALNAME_EXISTS "Your name has been taken, please rejoin with another name"
#define INVALID_NICKNAME "Nickname must be a max of 10 characters"
#define INVALID_HOSTNAME "Hostname must be a max of 10 characters"
#define INVALID_REALNAME "Realname must be a max of 20 characters"
#define INVALID_MESSAGE "Messages can be a maximum on 256 characters"

#define PARTIAL_DISCONNECTED_CLIENT "is no longer in our chatting session"
#define PARTIAL_GROUP_WELCOME_MESSAGE "Let us welcome a new user: "

#endif