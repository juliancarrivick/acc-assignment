/**
*	mnc_connection_helpers provides functions for running login on a connection
*	connection
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef MNC_CONNECTION_HELPERS_H
#define MNC_CONNECTION_HELPERS_H

#include <stdbool.h>

#include "mnc_connection.h"
#include "connection_pool.h"

/*
 * Validates a connection's join message
 */
bool validateConnectionJoinMessage(MncConnection *connection, char *messageData);

/*
 * A readHandler (see connection_handler.h), handles a client rechoosing
 * its nickname
 */
int handleNickNameRePicking(MncConnectionPool *pool, MncConnection *connection);

/*
 * Checks that a nickname is valid or not
 */
bool checkNickName(MncConnectionPool *pool, MncConnection *connection, char *nickName);

#endif