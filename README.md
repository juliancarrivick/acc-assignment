# Advanced Computer Communications 300 Semester 2 2015 Assignment (acc-assignment)
*Written by Julian Carrivick while at Curtin University*

acc-assignment is an implementation of a subset of IRC, containing a server and a client.
Both are included in this repository.

## Install Development Tools

Ensure `gcc` and `make` are installed.

This can typically be done on Linux by installing the `build-essential` package.

## Build

To build the server, run `make mnc_server`.

To build the client, run `make mnc_client`.

Alternatively, compile everything by running `make` or `make all`

To build in debug mode, run `make debug`. Please note, if the program has been
compiled earlier without debugging enabled, you will have to run `make clean` first.

## Usage

### Server

To run the server, ensure it has been built, then execute:

`./mnc_server <max number of clients> <timeout in seconds>`

### Client

To run the client, ensure it has been built, then execute 

`./mnc_client <server hostname or IP address> <server port>`

## Example Usage

Start the server with a max of 10 clients and a timeout od 60 seconds:
`./mnc_server 10 60`

Start two clients with the relevent details:
`./mnc_client localhost 52010`

On one client type `JOIN julz julianhome Julian Carrivick`.
You should get a welcome message:
`--- Server: Welcome to MNC! ---`

On the other client type `JOIN julz johnHome John Citizen`.
You should be told your nickname has already been taken and to enter another:
`--- Server: That nickname exists already, please rejoin with a different one ---`

Type `JOIN john johnHome John Citizen` to pick a unused nick name and you 
should be welcomed too:
`--- Server: Welcome to MNC! ---`

On the first client type `WHOIS john` to get information about John. It may
look a little like this:
`--- Server: john is: ---
--- Realname: John Citizen ---
--- Hostname: johnHome ---
--- Time Joined: 2015-10-24 13:49:49 ---`

On the second client, type `TIME`, you should get the time back:
`--- Server: 2015-10-24 13:50:55 ---`

Send a message from John to Julian:
`MSG julz Hi Julian!`

Julian should get a message:
`--- john: Hi Julian! ---`

Julian can reply back to everybody:
`MSG Hi John!`

John should get the message:
`--- julz: Hi John! ---`

John doesn't want to talk to Julian anymore, so he quits:
`QUIT Bye Julian!` and gets told:
`--- Server: Thanks for being with us for the past 10 seconds. Bye john! ---`

Julian gets the message: 
`--- john: Bye Julian! ---
--- Server: john is no longer in our chatting session ---`

He is so angry that John left that he leaves his terminal for two minutes and
gets timed out by the server:
`--- Your connection has timed out, please reconnect ---`