/**
*	unix_helpers provides functions to assist utilising system calls.
*	They are mostly wrapper functions
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef UNIX_HELPERS_H
#define UNIX_HELPERS_H 

#include <pthread.h>
#include <sys/time.h>


/*
 * pthread_create wrapper
 */
void Pthread_create(pthread_t *tid, const pthread_attr_t *attr,
					void * (*func)(void *), void *arg);


/*
 * pthread_mutex_init wrapper
 */
 void Pthread_mutex_init(pthread_mutex_t *mutex, const pthread_mutexattr_t *attr);

/*
 * pthread_mutex_destroy wrapper
 */
void Pthread_mutex_destroy(pthread_mutex_t *mutex);

/*
 * pthread_mutex_lock wrapper
 */
void Pthread_mutex_lock(pthread_mutex_t *mutex);

/*
 * pthread_mutex_unlock wrapper
 */
void Pthread_mutex_unlock(pthread_mutex_t *mutex);

/*
 * Create a thread safe version of localtime.
 * There is localtimne_r, however it is not part of the standard, so for 
 * partability, create our own verion
 */
struct tm *mnc_localtime_r(const time_t *unixTime, struct tm *result);

/*
 * Generate a timeval struct with the given values
 */
struct timeval generateTimeStruct(int seconds, int microSeconds);

#endif