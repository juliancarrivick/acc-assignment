/**
*	validation_helpers provides functions to validate mnc specific data
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#include <string.h>

#include "validation_helpers.h"
#include "global_config.h"

/*
 * Checks if the given string is a valid nickname
 */
bool validNickName(char *nickName)
{
	if (strlen(nickName) <= MAX_NICKNAME)
	{
		return true;
	}

	return false;
}

/*
 * Checks if the given string is a valid hostName
 */
bool validHostName(char *hostName)
{
	if (strlen(hostName) <= MAX_HOSTNAME)
	{
		return true;
	}

	return false;
}

/*
 * Checks if the given string is a valid realName
 */
bool validFullName(char *realName)
{
	if (strlen(realName) <= MAX_REALNAME)
	{
		return true;
	}

	return false;
}

/*
 * Checks if the given string is a valid message
 */
bool validMessage(char *message)
{
	if (strlen(message) <= MAX_MESSAGE)
	{
		return true;
	}

	return false;
}

/*
 * Checks if the given int is a valid number of clients
 */
bool validMaxNoClients(int noClients)
{
	if (1 <= noClients && noClients <= 10)
	{
		return true;
	}

	return false;
}

/*
 * Checks if the given int is a valid timeout
 */
bool validTimeout(int timeout)
{
	if (1 <= timeout && timeout <= 120)
	{
		return true;
	}

	return false;
}