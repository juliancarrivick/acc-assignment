/**
*	global_config provides global configuration values to both the server and 
*	client
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef GLOBAL_CONFIG_H
#define GLOBAL_CONFIG_H

#define STUDENT_ID 16164442

#define MAX_STRING 2047
#define MAX_STRING_ALLOC 2048
#define MAX_BUFFER 2048

#define MAX_NICKNAME 10
#define MAX_NICKNAME_ALLOC 11
#define MAX_HOSTNAME 10
#define MAX_HOSTNAME_ALLOC 11
#define MAX_REALNAME 20
#define MAX_REALNAME_ALLOC 21
#define MAX_MESSAGE 258
#define MAX_MESSAGE_ALLOC 259

/********************
 * INVALID MESSAGES *
 ********************/
#define INVALID_JOIN_COMMAND "Usage: JOIN <nickname> <hostname> <realname>"
#define INVALID_WHOIS_COMMAND "Usage: WHOIS <nickname>"
 #define COUNLDNT_FIND_WHOIS "Could not find specified user"
#define INVALID_MSG_COMMAND "Usage: MSG [<targetnickname>] <message>"
#define UNKNOWN_COMMAND "Unknown Command. Please try again"

#endif