/**
*	string_helpers provides functions to assist manipulating strings
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef STRING_HELPERS_H
#define STRING_HELPERS_H

#include <stdbool.h>
#include <time.h>

/*
 * Removes a sub string from the given string, placing it in the destination
 * and moving the bytes at the end to where the end of removal occurred.
 */
void removeSubString(char *dest, char *str, int startIndex, int endIndex);

/* 
 * Removes a line from a string, placing it in the destination
 * Leaves the \n in place
 */
int extractLineFromString(char *lineDest, char *str);

/*
 * Takes a line from a buffer, placing it in the destination
 * Essentially extractLineFromString but removes the \n
 */
int readLine(char *line, char *buffer);

/*
 * Checks if the given string starts with another string
 */
bool startsWith(const char *wholeString, const char *startingString);

/*
 * Given a time_t, prints it in human readable form
 */
void printDateTime(char *message, time_t *timeToPrint);

/*
 * Removes any white space from the start or end of a string
 */
void trimLeadingAndTrailingWhitespace(char *str);

/*
 * Counts the number of words in a string.
 */
int wordCount(char *str);

#endif