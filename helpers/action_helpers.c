/**
*	action helpers provides functions to assist in writing messages or exiting
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#include <stdio.h>
#include <stdlib.h>

#include "action_helpers.h"

/*
 * Exit due to a user command or failure 
 * e.g. Not providing a server address
 */
void softExit(const char *message)
{
	printf("%s\n", message);
	exit(1);
}

/*
 * Exit due to a fatal system call or other reason
 */
void fatalExit(const char *message)
{
	fprintf(stderr, "FATAL: %s\n", message);
	exit(1);
}

/*
 * Write the specified debug message to the console
 */
void printDebugMessage(const char *message)
{
	printf("%s\n", message);
}