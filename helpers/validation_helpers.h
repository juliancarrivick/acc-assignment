/**
*	validation_helpers provides functions to validate mnc specific data
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef VALIDATION_HELPER_H
#define VALIDATION_HELPER_H 

#include <stdbool.h>

/*
 * Checks if the given string is a valid nickname
 */
bool validNickName(char *nickName);

/*
 * Checks if the given string is a valid hostName
 */
bool validHostName(char *hostName);

/*
 * Checks if the given string is a valid realName
 */
bool validFullName(char *realName);

/*
 * Checks if the given string is a valid message
 */
bool validMessage(char *message);

/*
 * Checks if the given int is a valid number of clients
 */
bool validMaxNoClients(int noClients);

/*
 * Checks if the given int is a valid timeout
 */
bool validTimeout(int timeout);


#endif