/**
*	tcp_helpers provides functions to assist utilising sockets.
*	They are mostly wrapper functions
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#include <netdb.h>
#include <netinet/in.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>

#include "tcp_helpers.h"
#include "action_helpers.h"

#include "../helpers/global_config.h"

/*
 * Given an address and port, will generate the relevent socket address structure
 */
struct sockaddr_in *generateSocketAddress(int address, int port)
{
	struct sockaddr_in *serverAddress;
	serverAddress = (struct sockaddr_in *)calloc(1, sizeof(struct sockaddr));

	/* Setup Socket Address Structure */
	serverAddress->sin_family = AF_INET;
	serverAddress->sin_addr.s_addr = htonl(address);
	serverAddress->sin_port = htons(port);

	return serverAddress;
}

/**
 * getHostByName
 * Looks up a host with DNS, returning the result in the given struct
 * If cannot resolve host name, exits the program.
 */
int getHostByName(const char *host, struct in_addr *address)
{
	struct hostent *hostData;

	hostData = gethostbyname(host);

	if (hostData == NULL)
	{
		return 0;
	}

	*address = *((struct in_addr*)hostData->h_addr_list[0]);

	return 1;
}

/*
 * socket wrapper
 */
int tcpSocket()
{
	int socketFd;

	socketFd = socket(AF_INET, SOCK_STREAM, 0);

	if (socketFd < 0)
	{
		fatalExit("Could not create socket.");
	}

	return socketFd;
}

/*
 * setsockopt wrapper
 */
int SetSockOpt(int sockfd, int level, int optname, void *optval, socklen_t optlen)
{
	int returnValue;

	returnValue = setsockopt(sockfd, level, optname, optval, optlen);

	if (returnValue < 0)
	{
		fatalExit("Could not set socket option.");
	}

	return returnValue;
}

/*
 * Connects to the given file descriptor
 */
int tcpConnect(int fileDesc, struct sockaddr_in *socketAddress)
{
	int connectReturnValue;
	bool keepTrying;

	do
	{
		keepTrying = false;
		
		connectReturnValue = connect(fileDesc, (struct sockaddr*)socketAddress, sizeof(struct sockaddr));

		if (connectReturnValue < 0 && errno == EINTR)
		{
			keepTrying = true;
		}
		else if (connectReturnValue < 0)
		{
			fatalExit("Could not connect to specified host.");
		}

	} while (keepTrying);

	return connectReturnValue;
}

/*
 * listen wrapper
 */
int tcpListen(int listenQueue, int listenFileDesc, struct sockaddr_in *socketAddress)
{
	printDebug("Opening listening socket");

	if (bind(listenFileDesc, (const struct sockaddr*)socketAddress, sizeof(struct sockaddr_in)) < 0)
	{
		fatalExit("Could not bind socket to address");

	}

	if (listen(listenFileDesc, listenQueue) < 0)
	{
		fatalExit("Could not open listen socket.");
	}

	return listenFileDesc;
}

/*
 * accept wrapper
 */
int tcpAccept(int listenFileDesc, struct sockaddr_in *clientAddress, socklen_t *clientLength)
{
	int connectionFileDesc;

	connectionFileDesc = accept(listenFileDesc, (struct sockaddr*)clientAddress, clientLength);

	printDebug("Accepted new connection");

	if (connectionFileDesc < 0)
	{
		fatalExit("Unable to accept from file descriptor.");
	}

	return connectionFileDesc;
}

/*
 * select wrapper
 */
int Select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds,
           struct timeval *timeout)
{
	int readyDescriptors = select(nfds, readfds, writefds, exceptfds, timeout);

	if (readyDescriptors < 0)
	{
		fatalExit("Select was unable to complete.");
	}

	return readyDescriptors;
}

/*
 * read wrapper 
 */
int Read(int fileDesc, char *buffer, size_t count)
{
	int readReturnValue;
	bool keepTrying;

	do
	{
		keepTrying = false;

		readReturnValue = read(fileDesc, buffer, count);

		if (readReturnValue < 0)
		{
			if (errno == EINTR)
			{
				printDebug("Read interrupted, continuing.");
				keepTrying = true;
			}
			else
			{
				fatalExit("Fatal error when reading from socket.");
			}
		}
		#ifdef DEBUG
		else if (readReturnValue > 0)
		{
			printf("Read %d bytes from socket, buffer: %s\n", readReturnValue, buffer);
		}
		else
		{
			printf("Reading Socket has been closed by other end.\n");
		}
		#endif
	} while(keepTrying);

	return readReturnValue;
}

/*
 * write wrapper
 */
int Write(int fileDesc, const char *buffer, size_t count)
{
	int writeReturnValue;
	bool keepTrying;

	do
	{
		keepTrying = false;

		writeReturnValue = write(fileDesc, buffer, count);

		if (writeReturnValue < 0)
		{
			if (errno == EINTR)
			{
				printDebug("Write interrupted, continuing.");
				keepTrying = true;
			}
			else
			{
				printf("Errno: %d\n", errno);
				fatalExit("Fatal error when writing to socket.");
			}
		}
		#ifdef DEBUG
		else if (writeReturnValue > 0)
		{
			printf("Wrote %ld bytes to socket, buffer: %s\n", count, buffer);
		}
		else
		{
			printf("Writing Socket has been closed by other end.\n");
		}
		#endif
	} while(keepTrying);

	return writeReturnValue;
}

/*
 * Writes the given string to the given socket
 */
int WriteString(int fileDesc, const char *string)
{
	return Write(fileDesc, string, strlen(string));
}

/*
 * Writes the given string to the socket, appending a \n
 */
int WriteLine(int fileDesc, const char *buffer)
{
	char tempBuffer[MAX_STRING_ALLOC];

	strcpy(tempBuffer, buffer);
	strcat(tempBuffer, "\n");

	return Write(fileDesc, tempBuffer, strlen(tempBuffer));
}

/*
 * shutdown wrapper
 */
int Shutdown(int fileDesc, int how)
{
	int shutdownReturnVal;

	shutdownReturnVal = shutdown(fileDesc, how);

	if (shutdownReturnVal < 0)
	{
		fatalExit("Could not shutdown socket.");
	}

	return shutdownReturnVal;
}

/*
 * close wrapper
 */
int Close(int fileDesc)
{
	int closeReturnValue;
	bool keepTrying;

	do
	{
		keepTrying = false;

		closeReturnValue = close(fileDesc);

		if (closeReturnValue < 0)
		{
			if (errno == EINTR)
			{
				keepTrying = true;
			}
			else
			{
				fatalExit("Fatal Error when attempting to close socket");
			}
		}
		else
		{
			printDebug("Closed socket succesfully");
		}
	} while (keepTrying);

	return closeReturnValue;
}