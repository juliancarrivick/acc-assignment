/**
*	tcp_helpers provides functions to assist utilising sockets.
*	They are mostly wrapper functions
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef TCP_HELPERS_H
#define TCP_HELPERS_H

#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/select.h>

/*
 * Given an address and port, will generate the relevent socket address structure
 */
struct sockaddr_in *generateSocketAddress(int address, int port);

/**
 * getHostByName
 * Looks up a host with DNS, returning the result in the given struct
 * If cannot resolve host name, exits the program.
 */
int getHostByName(const char *host, struct in_addr *address);

/*
 * socket wrapper
 */
int tcpSocket();

/*
 * setsockopt wrapper
 */
int SetSockOpt(int sockfd, int level, int optname, void *optval, socklen_t optlen);

/*
 * Connects to the given file descriptor
 */
int tcpConnect(int fileDesc, struct sockaddr_in *socketAddress);

/*
 * listen wrapper
 */
int tcpListen(int listenQueue, int listenFileDesc, struct sockaddr_in *socketAddress);

/*
 * accept wrapper
 */
int tcpAccept(int listenFileDesc, struct sockaddr_in *clientAddress, 
              socklen_t *clientLength);

/*
 * select wrapper
 */
int Select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds,
           struct timeval *timeout);

/*
 * read wrapper 
 */
int Read(int fileDesc, char *buffer, size_t count);

/*
 * write wrapper
 */
int Write(int fileDesc, const char *buffer, size_t count);

/*
 * Writes the given string to the given socket
 */
int WriteString(int fileDesc, const char *buffer);

/*
 * Writes the given string to the socket, appending a \n
 */
int WriteLine(int fileDesc, const char *buffer);

/*
 * shutdown wrapper
 */
int Shutdown(int fileDesc, int how);

/*
 * close wrapper
 */
int Close(int fileDesc);

#endif