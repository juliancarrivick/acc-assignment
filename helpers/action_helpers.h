/**
*	action helpers provides functions to assist in writing messages or exiting
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#ifndef ACTION_HELPERS_H
#define ACTION_HELPERS_H

/*
 * If there is the dubug flag, define the following macro
 */
#ifdef DEBUG
	// Macro so that function jumps aren't added as overhead when not debugging.
	#define printDebug(msg) printDebugMessage(msg);
#else
	// If not debug, empty statement.
	#define printDebug(msg) ;
#endif

/*
 * Exit due to a user command or failure 
 * e.g. Not providing a server address
 */
void softExit(const char *message);

/*
 * Exit due to a fatal system call or other reason
 */
void fatalExit(const char *message);

/*
 * Write the specified debug message to the console
 */
void printDebugMessage(const char *message);

#endif