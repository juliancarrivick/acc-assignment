/**
*	string_helpers provides functions to assist manipulating strings
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "action_helpers.h"
#include "string_helpers.h"
#include "unix_helpers.h"
#include "global_config.h"

/*
 * Removes a sub string from the given string, placing it in the destination
 * and moving the bytes at the end to where the end of removal occurred.
 */
void removeSubString(char *dest, char *str, int startIndex, int endIndex)
{
	int subStrSize, strSize;

	strSize = strlen(str);

	if (startIndex < 0)
	{
		startIndex = 0;
	}
	
	if (endIndex > strSize)
	{
		endIndex = strSize;
	}

	subStrSize = endIndex - startIndex + 1;

	// Move substring into its own array and set null terminator
	strncpy(dest, &(str[startIndex]), subStrSize);
	dest[subStrSize] = '\0';

	// And move the left over bytes to the start of the original array.
	memmove(str, &(str[endIndex + 1]), strlen(&(str[endIndex + 1])) + 1);
}

/* 
 * Removes a line from a string, placing it in the destination
 * Leaves the \n in place
 */
int extractLineFromString(char *lineDest, char *str)
{
	int strLength, newLineIndex, result;

	newLineIndex = -1;
	result = -1;
	strLength = strlen(str);

	// Check the buffer for new line
	for (int i = 0; i < strLength; ++i)
	{
		if (str[i] == '\n')
		{
			newLineIndex = i;
			break;
		}
	}

	// If there is a newline, remove that line from the given string and place 
	// in the destination
	if (newLineIndex >= 0) 
	{
		removeSubString(lineDest, str, 0, newLineIndex);
		result = 1;
	}
	else
	{
		result = 0;
	}

	return result;
}

/*
 * Takes a line from a buffer, placing it in the destination
 * Essentially extractLineFromString but removes the \n
 */
int readLine(char *line, char *buffer)
{
	int result;

	// Attempt to get a line from the buffer, on successful extraction,
	// > 0 will be returned, which is the same return spec as this function
	result = extractLineFromString(line, buffer);

	// Remove the trailing new line
	if (line[strlen(line) - 1] == '\n')
	{
		line[strlen(line) - 1] = '\0';
	}

	return result;
}

/*
 * Checks if the given string starts with another string
 */
bool startsWith(const char *wholeString, const char *startingString)
{
	// Check the length of startingString of wholeString for startingString.
	return strncmp(wholeString, startingString, strlen(startingString)) == 0; 
}

/*
 * Given a time_t, prints it in human readable form
 */
void printDateTime(char *message, time_t *timeToPrint)
{
	struct tm timeStruct;

	mnc_localtime_r(timeToPrint, &timeStruct);

	strftime(message, MAX_STRING - 1, "%F %T", &timeStruct);
}

/*
 * Removes any white space from the start or end of a string
 */
void trimLeadingAndTrailingWhitespace(char *str)
{
	int startIndex, endIndex;

	startIndex = 0;
	endIndex = strlen(str) - 1;

	while (isspace(str[startIndex]))
	{
		startIndex++;
	}

	while(isspace(str[endIndex]))
	{
		endIndex--;
	}

	// Update the null terminator
	str[endIndex + 1] = '\0';

	// Move the whitespace removed string to the start of the array
	memmove(str, &str[startIndex], endIndex - startIndex + 2);
}


/*
 * Counts the number of words in ta string
 */
int wordCount(char *str)
{
	int count, stringlen;
	char tempString[MAX_STRING_ALLOC];

	strcpy(tempString, str);
	trimLeadingAndTrailingWhitespace(tempString);

	count = 1;
	stringlen = strlen(tempString);


	for (int i = 0; i < stringlen - 1; ++i)
	{
		if (tempString[i] == ' ' && tempString[i+1] != ' ')
		{
			count++;
		}
	}

	return count;
}