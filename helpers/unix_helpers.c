/**
*	unix_helpers provides functions to assist utilising system calls.
*	They are mostly wrapper functions
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Unit:		ACC300
*/

#include <stdio.h>
#include <time.h>

#include "unix_helpers.h"
#include "action_helpers.h"

/*
 * pthread_create wrapper
 */
void Pthread_create(pthread_t *tid, const pthread_attr_t *attr,
					void * (*func)(void *), void *arg)
{
	int pThreadReturn = pthread_create(tid, attr, func, arg);

	if (pThreadReturn != 0)
	{
		fatalExit("Could not create new thread.");
	}
}

/*
 * pthread_mutex_init wrapper
 */
void Pthread_mutex_init(pthread_mutex_t *mutex, const pthread_mutexattr_t *attr)
{
	if (pthread_mutex_init(mutex, attr) != 0)
		fatalExit("Could not initialise mutex");
}

/*
 * pthread_mutex_destroy wrapper
 */
void Pthread_mutex_destroy(pthread_mutex_t *mutex)
{
	if (pthread_mutex_destroy(mutex) != 0)
	{
		fatalExit("Could not destroy mutex");
	}
}

/*
 * pthread_mutex_lock wrapper
 */
void Pthread_mutex_lock(pthread_mutex_t *mutex)
{
	if (mutex != NULL && pthread_mutex_lock(mutex) != 0)
	{
		fatalExit("Could not destroy mutex");
	}
}

/*
 * pthread_mutex_unlock wrapper
 */
void Pthread_mutex_unlock(pthread_mutex_t *mutex)
{
	if (mutex != NULL && pthread_mutex_unlock(mutex) != 0)
	{
		fatalExit("Could not destroy mutex");
	}
}

/*
 * Create a thread safe version of localtime.
 * There is localtimne_r, however it is not part of the standard, so for 
 * partability, create our own verion
 */
struct tm *mnc_localtime_r(const time_t *unixTime, struct tm *result)
{
	static pthread_mutex_t localTimeMutex = PTHREAD_MUTEX_INITIALIZER;

	struct tm *tempTime;

	Pthread_mutex_lock(&localTimeMutex);

	tempTime = localtime(unixTime);

	// Copy result
	*result = *tempTime;

	Pthread_mutex_unlock(&localTimeMutex);

	return result;
}

/*
 * Generate a timeval struct with the given values
 */
struct timeval generateTimeStruct(int seconds, int microSeconds)
{
	struct timeval timeStruct;

	timeStruct.tv_sec = (long)seconds;
	timeStruct.tv_usec = (long)microSeconds;

	return timeStruct;
}