CC = gcc
CFLAGS = -Wall -pedantic -std=c99
LINKER_FLAGS = -lpthread

all: mnc_client mnc_server

debug: CFLAGS += -D DEBUG=1 -g
debug: all

######################
# HELPER COMPILATION #
######################

HELPER_FOLDER = helpers
HELPER_OBJ = tcp_helpers.o unix_helpers.o action_helpers.o string_helpers.o validation_helpers.o

tcp_helpers.o: $(HELPER_FOLDER)/tcp_helpers.c $(HELPER_FOLDER)/tcp_helpers.h
	$(CC) $(CFLAGS) -c $(HELPER_FOLDER)/tcp_helpers.c 

unix_helpers.o: $(HELPER_FOLDER)/unix_helpers.c $(HELPER_FOLDER)/unix_helpers.h
	$(CC) $(CFLAGS) -c $(HELPER_FOLDER)/unix_helpers.c 

action_helpers.o: $(HELPER_FOLDER)/action_helpers.c $(HELPER_FOLDER)/action_helpers.h
	$(CC) $(CFLAGS) -c $(HELPER_FOLDER)/action_helpers.c 

string_helpers.o: $(HELPER_FOLDER)/string_helpers.c $(HELPER_FOLDER)/string_helpers.h
	$(CC) $(CFLAGS) -c $(HELPER_FOLDER)/string_helpers.c 

validation_helpers.o: $(HELPER_FOLDER)/validation_helpers.c $(HELPER_FOLDER)/validation_helpers.h
	$(CC) $(CFLAGS) -c $(HELPER_FOLDER)/validation_helpers.c 

######################
# CLIENT COMPILATION #
######################

CLIENT_FOLDER = client
CLIENT_OBJ = mnc_client.o client_helpers.o

mnc_client: $(HELPER_OBJ) $(CLIENT_OBJ) $(HELPER_FOLDER)/global_config.h
	$(CC) $(CFLAGS) $(LINKER_FLAGS) $(CLIENT_OBJ) $(HELPER_OBJ) -o mnc_client

mnc_client.o: $(CLIENT_FOLDER)/mnc_client.c $(CLIENT_FOLDER)/mnc_client.h
	$(CC) $(CFLAGS) -c $(CLIENT_FOLDER)/mnc_client.c

client_helpers.o: $(CLIENT_FOLDER)/client_helpers.c $(CLIENT_FOLDER)/client_helpers.h
	$(CC) $(CFLAGS) -c $(CLIENT_FOLDER)/client_helpers.c

mnc_test: $(HELPER_OBJ) mnc_client_test.o client_helpers.o $(HELPER_FOLDER)/global_config.h
	$(CC) $(CFLAGS) $(LINKER_FLAGS) mnc_client_test.o client_helpers.o $(HELPER_OBJ) -o mnc_test

######################
# SERVER COMPILATION #
######################

SERVER_OBJ = mnc_server.o connection_pool.o mnc_connection.o connection_handler.o command_handler.o pool_helpers.o mnc_connection_helpers.o
SERVER_FOLDER = server

mnc_server: $(HELPER_OBJ) $(SERVER_OBJ) $(SERVER_FOLDER)/server_config.h $(HELPER_FOLDER)/global_config.h
	$(CC) $(CFLAGS) $(LINKER_FLAGS) $(SERVER_OBJ) $(HELPER_OBJ) -o mnc_server

mnc_server.o: $(SERVER_FOLDER)/mnc_server.c $(SERVER_FOLDER)/mnc_server.h
	$(CC) $(CFLAGS) -c $(SERVER_FOLDER)/mnc_server.c

connection_pool.o: $(SERVER_FOLDER)/connection_pool.c $(SERVER_FOLDER)/connection_pool.h
	$(CC) $(CFLAGS) -c $(SERVER_FOLDER)/connection_pool.c

mnc_connection.o: $(SERVER_FOLDER)/mnc_connection.c $(SERVER_FOLDER)/mnc_connection.h
	$(CC) $(CFLAGS) -c $(SERVER_FOLDER)/mnc_connection.c

connection_handler.o: $(SERVER_FOLDER)/connection_handler.c $(SERVER_FOLDER)/connection_handler.h
	$(CC) $(CFLAGS) -c $(SERVER_FOLDER)/connection_handler.c

command_handler.o: $(SERVER_FOLDER)/command_handler.c $(SERVER_FOLDER)/command_handler.h
	$(CC) $(CFLAGS) -c $(SERVER_FOLDER)/command_handler.c

pool_helpers.o: $(SERVER_FOLDER)/pool_helpers.c $(SERVER_FOLDER)/pool_helpers.h
	$(CC) $(CFLAGS) -c $(SERVER_FOLDER)/pool_helpers.c

mnc_connection_helpers.o: $(SERVER_FOLDER)/mnc_connection_helpers.c $(SERVER_FOLDER)/mnc_connection_helpers.h
	$(CC) $(CFLAGS) -c $(SERVER_FOLDER)/mnc_connection_helpers.c

#####################
# CLEANUP FUNCTIONS #
#####################

clean:
	rm -f mnc_server mnc_client $(HELPER_OBJ) $(SERVER_OBJ) $(CLIENT_OBJ)